import { Component, OnInit } from '@angular/core';
import { Header } from './models/header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public header: Header;
  constructor() {
    this.header = {
      icon: {
        img: "../assets/images/DJ HUB LOGO.png",
        name: "dj hub logo",
      },
      links: ["Musica", "Equipo"],
    };
  }

  ngOnInit(): void {
  }

}
