export interface Header {
  icon: Icon,
  links: Array <string>
}

export interface Icon {
  img: string;
  name: string;
}
