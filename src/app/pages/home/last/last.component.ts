import { Last } from './../models/home';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-last',
  templateUrl: './last.component.html',
  styleUrls: ['./last.component.scss']
})
export class LastComponent implements OnInit {

    @Input() last!: Last;
    public dsp: boolean = true;
    constructor() { }

    ngOnInit(): void {
    }
    onButtonClick() : void {
      this.dsp = !this.dsp;
  }
}
