export interface Home {
  intro: Intro;
  middle: Middle;
  last: Last;
}

export interface Intro {
  img: Image;
  info: string;
}

export interface Middle{
  img: Image;
  content: Array<Links>;
}

export interface Last{
  img: Image;
  content: Array<Links>;
}


export interface Image {
  img: string;
  name: string;
}

export interface Links {
  img: Image;
  link: string;
}
