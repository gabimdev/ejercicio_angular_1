import { Home } from './models/home';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public home: Home;
  constructor() {
    this.home = {
      intro: {
        img: {
          img: "../assets/images/DJ-Tools.png",
          name: "dj tools"
        },
        info: "Links para Djs"
      },
      middle: {
        img: {
          img: "https://d27t0qkxhe4r68.cloudfront.net/images/logos/vinyl1.png?1491044400",
          name: "disco"
        },
        content: [
          {
            img: {
              img: "https://media-exp1.licdn.com/dms/image/C560BAQEXFotXWc1xZA/company-logo_200_200/0/1519855867057?e=2159024400&v=beta&t=YoHlJajOkSV8dkl1UXu8_vsJqPR-YqoEV8Y52FURl1c",
              name: "beatport"
            },
            link: "https://www.beatport.com/"
          },
          {
            img: {
              img: "https://geo-w-static.traxsource.com/img/share_logo.png",
              name: "traxsource"
            },
            link: "https://www.traxsource.com/"
          },
          {
            img: {
              img: "https://cdn1.dontpayfull.com/media/logos/size/300x300/junodownload.com.jpg?v=20200414134418996827",
              name: "juno download"
            },
            link: "https://www.junodownload.com/"
          },
          {
            img: {
              img: "http://www.tiumag.com/wp-content/uploads/TIU-noticias-Bandcamp-numbers-2016-2017-2-705x529.jpg",
              name: "Bandcamp"
            },
            link: "https://bandcamp.com/"
          },
        ]
      },
      last: {
        img: {
          img: "../assets/images/technics-sl1210mk7-turntable-spm.png",
          name: "equipo"
        },
        content: [
          {
            img: {
              img: "https://www.cutoff.es/themes/sns_nova/images/logo.jpg",
              name: "Cutoff"
            },
            link: "https://www.cutoff.es/"
          },
          {
            img: {
              img: "https://thumbs.static-thomann.de/thumb/thumb640x/pics/images/comp/history/audioprof-rs.jpg",
              name: "Thomann"
            },
            link: "https://www.thomann.de/es/index.html"
          },
          {
            img: {
              img: "https://cdn.microfusa.com/tienda/pub/media/logo/stores/1/logoNEW.png",
              name: "Microfusa"
            },
            link: "https://microfusa.com/tienda/"
          },
          {
            img: {
              img: "https://media.djmania.net/assets/img/logodjmania.svg",
              name: "Dj Mania"
            },
            link: "https://djmania.es/"
          },
        ]
      }
    }
  }

  ngOnInit(): void {
  }

}

