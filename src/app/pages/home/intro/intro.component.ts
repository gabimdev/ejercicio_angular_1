import { Intro } from './../models/home';


import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

@Input() intro!: Intro;
  constructor() {
  }

  ngOnInit(): void {
  }

}
