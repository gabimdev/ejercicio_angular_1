import { Middle } from './../models/home';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-middle',
  templateUrl: './middle.component.html',
  styleUrls: ['./middle.component.scss']
})
export class MiddleComponent implements OnInit {

  @Input () middle!: Middle;
  public dsp: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  onButtonClick() : void {
    this.dsp = !this.dsp;
  }
}
